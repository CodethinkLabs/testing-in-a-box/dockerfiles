#!/bin/bash

mkdir -p /etc/openqa/
/apply_template.py /templates/client.conf /etc/openqa/client.conf
/apply_template.py /templates/workers.ini /etc/openqa/workers.ini

git config --global --add safe.directory /data/tests/*

set -e

if [[ -z $OPENQA_WORKER_INSTANCE ]]; then
  OPENQA_WORKER_INSTANCE=1
fi

mkdir -p "/var/lib/openqa/pool/${OPENQA_WORKER_INSTANCE}/"
chown -R _openqa-worker /var/lib/openqa/pool/

# Install test distribution dependencies
# if [[ -z $TEST_DISTRI_DEPS ]] ; then
#     find -L "/var/lib/openqa/share/tests" -maxdepth 2 -type f -executable -name 'install_deps.*' -exec {} \;
# else
#     zypper -n --gpg-auto-import-keys install $TEST_DISTRI_DEPS
# fi

su _openqa-worker -c "/usr/share/openqa/script/worker --verbose --instance \"$OPENQA_WORKER_INSTANCE\" --apikey $OPENQA_KEY --apisecret $OPENQA_SECRET"
