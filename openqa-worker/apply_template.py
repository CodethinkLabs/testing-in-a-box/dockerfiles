#!/usr/bin/env python3

import os
import argparse
import jinja2

parser = argparse.ArgumentParser(description='Apply a jinja2 template using environment variables.')
parser.add_argument("infile", help="The input jinja2 template")
parser.add_argument("outfile", help="The file the jinja2 template will be outputted to")
args = parser.parse_args()

with open(args.infile, 'r') as template_:
    template = jinja2.Template(template_.read())
    config = template.render(os.environ)
    with open(args.outfile, 'w') as fh:
        fh.write(config)
