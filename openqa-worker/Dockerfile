FROM fedora:41

RUN dnf install -y git openqa-worker vim patch qemu-device-display-virtio-vga qemu-device-display-virtio-gpu qemu-device-display-virtio-gpu-pci ffmpeg-free

# Copy over files to allow add the HTTP backend and apply some patches
# The first patch allows empty SSH passwords for the SSH console
# The second prevents the URL of the OpenQA tests repository from having its authentication info stripped
ARG OS_AUTOINST_SHA
RUN git clone https://github.com/os-autoinst/os-autoinst && \
    cd os-autoinst && \
    cp consoles/* /usr/lib/os-autoinst/consoles/ && \
    cp backend/* /usr/lib/os-autoinst/backend/ && \
    cp OpenQA/Isotovideo/Utils.pm /usr/lib/os-autoinst/OpenQA/Isotovideo/Utils.pm && \
    cd .. && rm -r os-autoinst

# Clone test utilities so that the runner can use them locally
RUN git clone https://gitlab.com/CodethinkLabs/testing-in-a-box/tests/simphoney.git /opt/simphoney && \
    git -C /opt/simphoney pull --all

RUN git clone https://gitlab.com/CodethinkLabs/testing-in-a-box/tests/audio-testing.git /opt/audio-testing && \
    git -C /opt/audio-testing pull --all

# Install pandas, altair, iproute2, canutils, Python, CANtools and screen
RUN dnf install -y iproute can-utils pipewire pipewire-utils bluez bluez-deprecated bluez-libs bluez-tools dbus usbutils python python-pip python-devel python-dbus python-pydbus python-requests python-flask screen sshpass unzip alsa-utils sox gcc g++ && \
    pip3 install cantools==37.2.0 python-can==3.3.2 setuptools jinja2 matplotlib==3.9.2 numpy==2.1.2 scipy==1.14.1

WORKDIR /
ENV OPENQA_SERVER=
ENV OPENQA_WORKER_BACKEND=
ENV OPENQA_WORKER_CLASS=
ENV OPENQA_KEY=
ENV OPENQA_SECRET=
ADD templates /templates
ADD apply_template.py /apply_template.py
ADD entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/bin/bash"]
