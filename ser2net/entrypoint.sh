#!/bin/bash

sed -i s/ttyS0/$SER2NET_DEVICE/ /etc/ser2net/ser2net.yaml

if [[ -v SER2NET_BAUD_RATE ]]; then
    sed -i s/9600/$SER2NET_BAUD_RATE/ /etc/ser2net/ser2net.yaml
fi

ser2net -b -d -l -c /etc/ser2net/ser2net.yaml
