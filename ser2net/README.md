# Codethink's ser2net container
This is a Docker container which contains a single [`ser2net`](https://github.com/cminyard/ser2net) instance. This container was created to aid in Codethink's LAVA deployments.

It is only capable of exposing serial devices over telnet.

## Usage
When the container is run, `ser2net` will be configured at run-time via the following environment variables:
- `SER2NET_PORT` - The port that ser2net will be listening on for telnet communication _inside_ the container, defaults to `6001`. It is recommended that you simple expose a different port on the outside of the container and map it to `6001` on the inside.
- `SER2NET_DEVICE` - This variable **must be defined**. It specifies the name of the serial device in `/dev` to expose via telnet. If you want to expose a serial device `/dev/ttyUSB0`, then `SER2NET_DEVICE` should be set to `ttyUSB0`. See the example below.

```
docker run --name "codethink_ser2net" --privileged -v "$2:$2" --env "SER2NET_DEVICE=$(basename $2)" --publish "6001:6001" -d registry.gitlab.com/codethinklabs/testing-in-a-box/ct-ser2net:main
```

If you are running this are part of a lava worker deployment, and the worker and ser2net container are on the same host, then you need not publish the ports. Instead just attach both containers to the same network like so:
```
docker run --name "codethink_ser2net" --network=lava_worker --privileged -v "$2:$2" --env "SER2NET_DEVICE=$(basename $2)" -d registry.gitlab.com/codethinklabs/testing-in-a-box/ct-ser2net:main
```
