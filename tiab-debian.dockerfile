FROM debian:latest

RUN export DEBIAN_FRONTEND=noninteractive && apt update && apt -y full-upgrade
RUN export DEBIAN_FRONTEND=noninteractive && apt -y install python3 python3-pip tox git mypy
