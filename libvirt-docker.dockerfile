FROM python:3-slim

RUN apt-get update && apt-get -y install lsb-release wget gpg make gcc ssh
RUN wget -O - https://apt.releases.hashicorp.com/gpg | gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
RUN echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list
RUN apt-get update && apt-get install -y qemu-system \
    kmod \
    libvirt-daemon-system \
    libvirt-dev \
    vagrant

ENV VAGRANT_DEFAULT_PROVIDER=libvirt

RUN /usr/sbin/libvirtd -d
RUN /usr/sbin/virtlogd -d
RUN vagrant plugin install vagrant-libvirt
RUN vagrant plugin install vagrant-hostmanager
RUN vagrant plugin install vagrant-mutate
