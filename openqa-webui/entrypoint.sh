#!/bin/sh

if [ -z "${GIT_GLOBAL_EMAIL}" ] || [ -z "${GIT_GLOBAL_USERNAME}" ];
then
    echo "GIT_GLOBAL_EMAIL and GIT_GLOBAL_USERNAME must be defined."
    exit 1
fi

git config --global user.name "${GIT_GLOBAL_USERNAME}"
git config --global user.email "${GIT_GLOBAL_EMAIL}"

mkdir -p /data/conf

/apply_template.py /templates/database.ini /data/conf/database.ini

if [ $OPENQA_DEMO = true ]
then
    echo "Using demo config"
    /apply_template.py /templates/openqa.demo.ini /data/conf/openqa.ini
else
    echo "Using reguler OAuth2 config"
    /apply_template.py /templates/openqa.ini /data/conf/openqa.ini
fi

exec /root/run_openqa.sh
