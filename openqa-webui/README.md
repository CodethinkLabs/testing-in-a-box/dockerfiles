# OpenQA Webui container

This is the Dockerfile for Codethinks internal OpenQA webui.  It includes codethink
branding, and default config files built from environment variables.

Some environment variables must be set when using the container:

- `OPENQA_URL` - The URL this openqa instance will be available at
- `GIT_GLOBAL_USERNAME` - The `user.name` to be used in `git config --global`
- `GIT_GLOBAL_EMAIL` - The `user.email` to be used in `git config --global`

If using an external postgres database, you'll want to set the following variables

- `POSTGRES_DB` (default: openqa)
- `POSTGRES_HOSTNAME` (default: db)
- `POSTGRES_USER` (default: openqa)
- `POSTGRES_PASSWORD` (default: openqa)

If using oauth2 for your authentication, you'll need to set the following:

- `OPENQA_OAUTH2_NAME` - short name of auth provider
- `OPENQA_OAUTH2_KEY` - client id in oauth2 provider
- `OPENQA_OAUTH2_TOKEN` - token for oauth2 provider
- `OPENQA_OAUTH2_AUTH_URL` - url to authenticate against
- `OPENQA_OAUTH2_TOKEN_URL` - url to exchange tokens with
- `OPENQA_OAUTH2_USER_URL` - url to scoop user data from
- `TOKEN_SCOPE` - OAuth2 token scope

If you're using fake authentication for demo purposes you will need to set `OPENQA_DEMO` to `true`.
